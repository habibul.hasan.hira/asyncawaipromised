const users =[
    {
        name:"hria",
        id:1,
        schoolId:101
    }, {
        name:"jessical",
        id:2,
        schoolId:333
    }, {
        name:"siam",
        id:3,
        schoolId:666
    }];

    const grades=[
        {
            id:1,
            schoolId:101,
            grade:86
        },
        {
            id:3,
            schoolId:666,
            grade:89
        },
        {
            id:2,
            schoolId:333,
            grade:89
        },
        {
            id:2,
            schoolId:333,
            grade:89
        }
    ]

    const getGrades=(schoolId)=>{
      return new Promise((resolve, reject)=>{
          ///
          resolve(grades.filter((grade)=>grade.schoolId===schoolId))
      })

    }
    const getUser=(id)=>{
        return new Promise ((resolve,reject)=>{
            const user = users.find((user)=>user.id===id);
            if(user) resolve(user);
            else reject("unable to find user by this id");

        })
    }

        // getGrades(333).then((data)=>{
        //     console.log(data)
        // }).catch((error)=>{
        
        //     console.log(error);
        // })

        const getStatus=(userId)=>{
          return getUser(userId).then((user)=>{
              return getGrades(user.schoolId)
          }).then((grades)=>{
              let avg=0;
              if(grades.length>0){
                  avg=grades.map((grade)=>grade.grade).reduce((a,b)=>{return a+b})/grades.length;

              }
              console.log(avg);
          })
        }

    // getUser(100)
    // .then((data)=>{
    //   console.log(data)
    // })
    // .catch((e)=>{console.log(e)})

   // getStatus(2);

   
   const getStatusAlt= async(userId)=>
   {
      const user= await getUser(userId);
      const grades= await getGrades(user.schoolId);

      let avg=0;
      if(grades.length>0){
          avg=grades.map((grade)=>grade.grade).reduce((a,b)=>{return a+b})/grades.length;

      }
      console.log(avg);
     
   }

  getStatusAlt(2);
