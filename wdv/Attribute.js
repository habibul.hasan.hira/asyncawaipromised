/*global window jQuery $ CommonMethods document wNumb noUiSlider*/
function AttributeSelection() {
    this.commonMethod = new CommonMethods();
}
// Varaible List
AttributeSelection.prototype.parentAtt = "";
AttributeSelection.prototype.slider_no = false;
AttributeSelection.prototype.previewDataList = {};
AttributeSelection.prototype.attributeTopicList = [];// holds all information about treeView 
AttributeSelection.prototype.attributeTopicObject = [];
AttributeSelection.prototype.attributeGrid = null;
AttributeSelection.prototype.CheckedAttributeItemList = {};
AttributeSelection.prototype.reportDataList = {};
AttributeSelection.prototype.attributeInHirarchy = [];
AttributeSelection.prototype.currentAttributeInmodal;
// Get Attribute List from Database
AttributeSelection.prototype.getAttributeList = function (end) {
    var root = this;
    jQuery.ajax({
        type: "GET",
        url: "../api/PADDSLiteReport/getAttributeSet?dsnName=" + window.getParameterByName("dsnName"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var topicList = data;

            for (var i = 0; i < topicList.length; i++) {
                var dataSet = {};
                dataSet.text = topicList[i].topicFolderDescr;
                dataSet.href = "#parent" + (i + 1);
                dataSet.selectable = false;


                var childSet = [];
                for (var j = 0; j < topicList[i].childs.length; j++) {
                    var childNode = {}; //childDBName = [];

                    childNode.attributePkey = topicList[i].childs[j].topicsPkey;
                    childNode.text = topicList[i].childs[j].topicName;
                    childNode.dbName = topicList[i].childs[j].dbName;
                    childNode.groupBaseTopicPkey = topicList[i].childs[j].groupBaseTopicPkey;
                    childNode.href = "#child" + (i + 1);
                    //childDBName.push(topicList[i].childs[j].dbName);
                    childNode.tags = (topicList[i].childs[j].isGroupedTopic === 1) ? ["G"] : [" "];
                    if (root.previewDataList[childNode.text] !== undefined) {
                        if (root.previewDataList[childNode.text].hasOwnProperty('selectedItems')) {

                            childNode.tags[1] = [root.previewDataList[childNode.text].selectedItems.length.toString()]
                        }

                    }
                    childNode.isGroupedTopic = topicList[i].childs[j].isGroupedTopic;

                    childNode.dbSelectColumns = topicList[i].childs[j].dbSelectColumns;
                    childSet.push(childNode);
                }
                dataSet.nodes = childSet;
                root.attributeTopicList.push(dataSet);
            }
            root.generateAttributeTree();    // Load tree-view

            end(1);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $(".error1").css("visibility", "visible");
            $(".error1").text(errorThrown);
        }
    });
};

AttributeSelection.prototype.getUpdatedDataforTreeView = function () {

    if ((Object.keys(this.previewDataList).length > 0)) {
        for (i = 0; i < this.attributeTopicList.length; i++) {
            for (j = 0; j < this.attributeTopicList[i].nodes.length; j++) {
                var nodeName = this.attributeTopicList[i].nodes[j].text;
                if (this.previewDataList.hasOwnProperty(nodeName) && this.previewDataList[nodeName].selectedItems !== undefined) {

                    var selectionCount = this.previewDataList[nodeName].selectedItems.length;
                    if (selectionCount > 0) {
                        this.attributeTopicList[i].nodes[j].tags[1] = '' + selectionCount;
                    }


                }
                else {
                    this.attributeTopicList[i].nodes[j].tags[1] = '';
                }
            }

        }

    }

}

AttributeSelection.prototype.removeCheckBoxFromParentNode = function () {
    return;
    for (i = 0; i < $(".glyphicon-minus").length; i++) {
        $($(".glyphicon-minus")[i]).siblings(".check-icon").addClass('hidePermanently').hide();
    }
    for (i = 0; i < $(".glyphicon-plus").length; i++) {
        $($(".glyphicon-plus")[i]).siblings(".check-icon").addClass('hidePermanently').hide();
    }


}
// Generate Tree structure of Attributes
//Calls bootstrap-treeview line number 487
AttributeSelection.prototype.generateAttributeTree = function () {
    var root = this;
    var headatt;
    $("#treeview-checkable").treeview({
        data: root.attributeTopicList,
        showIcon: true,
        showCheckbox: true,
        showBorder: false,
        showTags: true,
        onNodeSelected: function (event, node) {
            root.onSelection(node)
        },
        onNodeChecked: function (event, node) {
            root.onChecked(node)
        },
        onNodeUnchecked: function (event, node) {
            root.onUnchecked(node)
        },
        onNodeCollapsed: function (event, node) {
            root.removeCheckBoxFromParentNode();
            event.stopPropagation();
        }
    });

};

AttributeSelection.prototype.onSelection = function (node) {
    document.getElementById("search").value = "";
    this.selectedAttributeItems();
    this.getUpdatedDataforTreeView();
    headatt = node.text;
    this.head_att(node.text); // Set Title of Attribute items Window while selecting attribute
    this.loadAttributeItems(node);// Load items for selected attribute

}

AttributeSelection.prototype.onUnchecked = function (node) {
    $("li > span:contains(" + node.text + ")").parent().remove();
    delete this.previewDataList[node.text];
    var index = this.attributeInHirarchy.indexOf(node.text);
    delete this.attributeInHirarchy[index];
    this.getUpdatedDataforTreeView(node.text);
    this.removeTagFromAttTopicList(node.text);
}

AttributeSelection.prototype.onChecked = function (node) {
    if (node.href.indexOf("parent") === -1) {
        if (!this.previewDataList[node.text]) {
            this.previewDataList[node.text] = {};
        }
        if (!this.attributeTopicIsInHirarchy(node.text)) {
            this.previewDataList[node.text].isInHierarchy = true;
            this.moveAttributeToHierarchy(node);
        }
    }
    else { alert("Parent Item can not be added.. ") }


}

AttributeSelection.prototype.removeTagFromAttTopicList = function (nodeText) {

    for (var i = 0; i < this.attributeTopicList.length; i++) {
        for (var j = 0; j < this.attributeTopicList[i].nodes.length; j++) {
            if (this.attributeTopicList[i].nodes[j].text === nodeText)
                this.attributeTopicList[i].nodes[j].tags[1] = [""];
        }
    }

}

AttributeSelection.prototype.loadAttributeItemOnSelection = function (data, attributeName) {

    var attributeItemList = data;
    this.attributeTopicObject = [];
    for (var i = 0; i < attributeItemList.length; i++) {
        var dataSet = {};
        dataSet.attributePkey = attributeItemList[i].pkey;
        dataSet.attributeDescr = attributeItemList[i].displayText;
        this.attributeTopicObject.push(dataSet);
    }
    this.createSlider(); // create range slider 
    $('#' + "inclu").prop('checked', true);// Make include bydeafult selected;
    if (this.previewDataList !== null) {
        for (property in this.previewDataList) {
            // $("li:contains('" + property + "')").find("span.glyphicon-unchecked").removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        }
        if (Object.getOwnPropertyNames(this.previewDataList).length != 0) {
            this.loadPreviousItemSelection(attributeName); // Load Previous item selection           
        }

    }
}
// Load attribute items
AttributeSelection.prototype.loadAttributeItems = function (attributeNode) {
    var root = this;
    var attributeName = attributeNode.text;
    var isGroup = attributeNode.isGroupedTopic === 1;
    $.LoadingOverlay("show");
    jQuery.ajax({
        type: "POST",
        url: "../api/PADDSLiteReport/AllIndividualAttributeItems",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            attributeName: isGroup ? attributeNode.text : attributeNode.dbName,
            isGroupedTopic: isGroup,
            dsnName: window.getParameterByName("dsnName"),
            dbSelectColumns: attributeNode.dbSelectColumns
        }),
        success: function (data) {
            $.LoadingOverlay("hide");
            root.loadAttributeItemOnSelection(data, attributeName);
        },
        error: function (textStatus, errorThrown) {
            $.LoadingOverlay("hide");
            Materialize.toast(textStatus.responseJSON.ExceptionMessage, 2000, "rounded");
        }
    });

};
// Create slider below of attribute items
AttributeSelection.prototype.createSlider = function () {
    var slider = document.getElementById("range-input");
    var count;
    if (this.slider_no) {
        slider.noUiSlider.destroy();
    }
    var settings = {
        start: [0, this.attributeTopicObject.length],
        connect: true,
        step: 1,
        range: {
            "min": 0,
            "max": this.attributeTopicObject.length
        },
        format: wNumb({
            decimals: 0
        })
    };
    noUiSlider.create(slider, settings);
    this.slider_no = true;
    var root = this;
    slider.noUiSlider.on("update", function (values, handle) {
        count = Math.ceil(values[handle]);
        // Load attribute after clicking attributes from tree
        root.attributeGrid = root.commonMethod.loadViaSlickGrid("attribute", root.attributeTopicObject.slice(0, count), root.parentAtt);
        // values = this.commonMethod.getCheckedItems(this.attributeGrid);       
    });
};

// Change header text of attribute while selecting
AttributeSelection.prototype.head_att = function (att) {
    $("#headAtt").html("");
    var html2 = "<h5 class='white-text' id='headAtt'>Attributes Items [" + att + "]</h5>";
    $(html2).appendTo("#headAtt");
    this.parentAtt = att;
};

//data-target='#query_settings'
// Move attributes to Hierarchy
AttributeSelection.prototype.hide = function (htmlElement, event) {
    var ItmName = htmlElement.textContent;
    var node = htmlElement.dataset.example
    jsonNode = JSON.parse(node);
    var nId = jsonNode.nodeId;
    $($("#treeview-checkable [data-nodeId='" + nId + "']")).click()
}
AttributeSelection.prototype.show = function (htmlElement, event) {

    var node = htmlElement.dataset.example
    jsonNode = JSON.parse(node);
    var attributeText = jsonNode.text;
    this.currentAttributeInmodal = attributeText;
    $('#query_settings').modal();
}
AttributeSelection.prototype.moveAttributeToHierarchy = function (node) {

    var nodeStr = JSON.stringify(node)
    var html = "<li><span class='text' onclick='wdv.attribItemList.hide(this,event)' data-example='" + nodeStr
            + "' data-example2='1234' style=' cursor: pointer' >" + node.text + "</span><div class='tools'>"
    html += "<a class='up_sub right'><p><i class='material-icons'>keyboard_arrow_up</i></p></a>"
    html += "<a class='down_sub right'><p><i class='material-icons'>keyboard_arrow_down</i></p></a>"
    html += "<a class='up_all right'><p><i class='material-icons'>vertical_align_top</i></p></a>"
    html += "<a class='down_all right'><p><i class='material-icons'>vertical_align_bottom</i></p></a>"
    html += "<a class='att_set right' data-toggle='query_settings'  data-example='" + nodeStr
        + "'  onclick='wdv.attribItemList.show(this,event)'  ><p><i class='material-icons '>settings</i></p></a>"
    html += "</div></li>";
    $(html).appendTo("#att_list");
    //html += "<a class='att_set right' data-toggle='modal'  ><p><i class='material-icons '>settings</i></p></a>"
};

AttributeSelection.prototype.moveAttributeTxtToHierarchy = function (nodeText) {

    var node = null;
    for (let i = 0; i < this.attributeTopicList.length; i++) {
        node = this.attributeTopicList[i].nodes.filter((nodeItem) => { return nodeItem.text === nodeText })
        if (node.length > 0) {

            // $( )[0] ).parent().data('nodeid')
            var checkedNodeList = $("#treeview-checkable li.node-checked").contents().not($("span"));
            for (var j = 0; j < checkedNodeList.length; j++) {
                if (checkedNodeList[j].textContent == nodeText) {
                    node[0].nodeId = $(checkedNodeList[j]).parent().data('nodeid');
                }
            }

            break;
        }
    }
    this.moveAttributeToHierarchy(node[0]);
};


// Add attributes to dropdown inside of attribute items
AttributeSelection.prototype.top_list = function () {
    throw "Not yet implemented yet";
    /*
    var container = document.getElementById("sel_top");
    var check = document.getElementById("sel_top").options[0];
    if (typeof check === "undefined") {
        container.innerHTML = "";
        for (var i = 0; i < this.attributeTopicList.length; i++) {
            for (var j = 0; j < this.attributeTopicList[i].nodes.length; j++) {
                var html = "<option value=" + this.attributeTopicList[i].nodes[j].text + ">" + this.attributeTopicList[i].nodes[j].text + "</option>";
                container.innerHTML += html;
            }
        }
        //$('select').material_select();
    }
    */
};
// This should be coming from PreviewData List
AttributeSelection.prototype.attributeTopicIsInHirarchy = function (attributeName) {

    // this.attributeInHirarchy = null;
    var attName = attributeName;
    var nums = document.getElementById("att_list");
    var listItem = nums.getElementsByTagName("li");

    var hierarchyArray = [];
    for (var i = 0; i < listItem.length; i++) {
        var att = listItem[i].outerText.split("\n")[0];
        if (this.attributeInHirarchy.indexOf(att) === -1)
            this.attributeInHirarchy.push(att);
    }
    if (this.attributeInHirarchy.indexOf(attName) !== -1)
        return true;
    else return false;

};

AttributeSelection.prototype.loadPreviousIncludeExcludeOption = function (attName) {
    if (typeof (this.previewDataList[attName]) !== "undefined") {
        if (this.previewDataList[attName].hasOwnProperty('isIncluded')) {
            $('#' + "exclu").prop('checked', true);
        }
    }
}
// Load Previous item selection
AttributeSelection.prototype.loadPreviousItemSelection = function (attributeName) {
    this.loadPreviousIncludeExcludeOption(attributeName);


    var itemList = [];
    var nodeText = attributeName;
    if (typeof (this.previewDataList[attributeName]) !== "undefined") {
        if (typeof (this.previewDataList[attributeName].selectedItems) !== "undefined") {
            for (var i = 0; i < this.previewDataList[attributeName].selectedItems.length; i++) {
                //itemList.push(this.previewDataList[attributeName].selectedItems[i].rowNumber);
                var indexOfPkey = this.attributeTopicObject.findIndex((itm) => {
                    return itm.attributePkey === this.previewDataList[attributeName].selectedItems[i]
                })
                itemList.push(indexOfPkey);
            }
            this.attributeGrid.setSelectedRows(itemList);//[1,2,4]
        }
        if (this.previewDataList[nodeText].isInHierarchy === true) {
            if (!this.attributeTopicIsInHirarchy(nodeText))
                this.moveAttributeTxtToHierarchy(nodeText);
        }

    }
};

// Remove selection of attribute items
// Unused
AttributeSelection.prototype.remove_selection = function () {
    $(".check_attribute:checkbox").prop("checked", false);
    this.previewDataList = [];
};
AttributeSelection.prototype.validateAttributeSelection = function () {
    var hasError = false;
    if ($.isEmptyObject(this.previewDataList)) {
        hasError = true;
        Materialize.toast("Please check at lelast one  Attribute ", 20000, "rounded");
    }

    return hasError;

}

// Filter Attribute Items
AttributeSelection.prototype.filter_items = function () {

    var searchString = $("#search").val();
    var attributeListBySearch = this.attributeTopicObject.filter(function (value2) {
        var value = {
            attributeDescr: value2.attributeDescr.trim().replace(")", "").replace("(", ""),
            attributePkey: value2.attributePkey
        }

        if (document.getElementById("contain").checked) {
            var filter = new RegExp(searchString, "gi");
            return value.attributeDescr.match(filter);
        }
        else if (document.getElementById("start").checked) {
            return (((value.attributeDescr.toLowerCase()).trim()).startsWith(searchString.toLowerCase()));
        }
        else {
            return (((value.attributeDescr.toLowerCase()).trim()).endsWith(searchString.toLowerCase()));
        }
    });
    //this.commonMethod.loadViaSlickGrid("attribute", AttributeListBySearch);
    this.refreshAttributeItems(attributeListBySearch);
};


// Generate Selected Attribute Items List
// Keyword: previewDataList
AttributeSelection.prototype.selectedAttributeItems = function () {

    var checkedAttItemParent = this.parentAtt; //$($('.node-selected')[0]).contents().not($("span")).text();
    if (checkedAttItemParent !== "") {
        if (typeof checkedAttItemParent === "string") {
            var selectedItemsInCard = this.commonMethod.getCheckedItemsPkey(this.commonMethod.getCheckedItems(this.attributeGrid));
            if (selectedItemsInCard.length == 0) {
                return;
            }
            if (!this.previewDataList[checkedAttItemParent]) {
                this.previewDataList[checkedAttItemParent] = {};
            }

            this.previewDataList[checkedAttItemParent].selectedItems =
                this.commonMethod.getCheckedItemsPkey(this.commonMethod.getCheckedItems(this.attributeGrid));

            this.previewDataList[checkedAttItemParent].isIncluded = $("input[name='groupAttributeShouldBeIncluded']:checked").val() == "include";
        }


    };
}
AttributeSelection.prototype.updatePreviewDataistFromHierarcy = function (nodetext) {
    if (nodetext !== "" || nodetext !== null || nodetext !== undefined) {
        var checkedAttItemParent = nodetext;
        if (checkedAttItemParent !== "") {
            if (typeof checkedAttItemParent === "string") {
                if (!this.previewDataList[checkedAttItemParent]) {
                    this.previewDataList[checkedAttItemParent] = {};
                }

                this.previewDataList[checkedAttItemParent].selectedItems =
                    this.commonMethod.getCheckedItemsPkey(this.commonMethod.getCheckedItems(this.attributeGrid));

            }
        }
    }


};

AttributeSelection.prototype.handleAlwaysNeverSelection = function (selectionName) {
    var values = [];
    var vauesWithSelectiontype = {
        SelectedattributeList: this.parentAtt,
        selectionType: selectionName,
        selectionValues: this.commonMethod.getCheckedItems(this.attributeGrid)
    };
    values.push(vauesWithSelectiontype);


};

//Generate table for selected Attribute items
// Keyword: preview-modal
AttributeSelection.prototype.previewSelectedattribute = function () {
    var attributeList = "", iconTag = "", itemList = "";

    //this.selectedAttributeItems();

    var sortedAttributeList = {};
    $(".todo-list li span").each(function (index, value) {
        sortedAttributeList[value.textContent] = index;
    });
    /*
    var swapPreviewDatalist = [];
    for (var key in this.previewDataList) {
        swapPreviewDatalist.push(key);
    }
    swapPreviewDatalist.sort(function (a, b) {
        return sortedAttributeList[a] > sortedAttributeList[b];
    });

    var temporaryPreviewDatalist = {};
    var root = this;
    swapPreviewDatalist.map(function (key) {
        temporaryPreviewDatalist[key] = root.
        
        
        [key];
    });
    this.previewDataList = temporaryPreviewDatalist;
    */
    for (var item in this.previewDataList) {
        this.previewDataList[item].positionInQuery = sortedAttributeList.hasOwnProperty(item) ?
            sortedAttributeList[item] : -1;

        if (typeof this.previewDataList[item].selectedItems !== "undefined") {
            iconTag = (this.previewDataList[item].selectedItems.length <= 9) ? "<i class='material-icons'>filter_" + this.previewDataList[item].selectedItems.length + "</i>" : "<i class='material-icons'>filter_9_plus</i>";

            itemList = "<table class='striped responsive-table'><thead><tr><th data-field='id'>Item Name</th></tr></thead><tbody>";
            for (var i = 0; i < this.previewDataList[item].selectedItems.length; i++) {
                itemList += "<tr><td>" + this.previewDataList[item].selectedItems[i].descr + "</td></tr>";
            }
            itemList += "</tbody></table>";
        }
        else {
            iconTag = "<i class='material-icons'>exposure_zero</i>";
            itemList = "<p>Default : All items are selected</p>";
        }
        attributeList += "<li><div class='collapsible-header'>" + iconTag + item + "</div>";
        attributeList += "<div class='collapsible-body'>" + itemList + "</div></li>";
    }

    $("#attributeList").html($(attributeList).clone());
    $(".collapsible").collapsible({
        accordion: false
    });
};
//}
AttributeSelection.prototype.applyCut = function () {

    var firstRowCount = $("#rowCount").val();
    var firstCondition = $('input[name=firstCondition]:checked').val();
    var aggregationType = $('input[name=aggregationType]:checked').val();
    var secondCondition = $('input[name=secondCondition]:checked').val()
    var secondRowCount = $("#rowCount_2").val();
    //Cut object for which atttribute list ? 
    var attributeName = this.currentAttributeInmodal;
    var cutObject = {
        attributeName: attributeName,

        firstRowCount: firstRowCount,
        firstCondition: firstCondition,
        aggregationType: aggregationType,
        secondRowCount: secondRowCount,
        secondCondition: secondCondition
    }
    if (this.attributeTopicIsInHirarchy(attributeName)) {
        this.previewDataList[attributeName].cutCondition = cutObject
    }

}

////////////////////////////////// Typescript conversion init
/// #433: Apply user selections
AttributeSelection.prototype.refreshAttributeItems = function (dataSet) {
    // Prepare
    var checkedAttItemParent = this.parentAtt;

    // Update attribute items
    this.attributeGrid = this.commonMethod.loadViaSlickGrid("attribute", dataSet, this.parentAtt);

    // User hasn't select anything yet, so no need to consider this in retrieve sql
    if (!this.previewDataList[checkedAttItemParent]) {
        return;
    }
    if (!this.previewDataList[checkedAttItemParent].selectedItems) {
        return;
    }

    // Update checkboxes on selected rows
    var attributeName = this.parentAtt;
    var itemList = [];
    for (var i = 0; i < this.previewDataList[attributeName].selectedItems.length; i++) {
        var indexOfPkey = dataSet.findIndex((itm) => {
            return itm.attributePkey === this.previewDataList[attributeName].selectedItems[i]
        })
        itemList.push(indexOfPkey);
    }
    if (itemList.length > 0) {
        this.attributeGrid.setSelectedRows(itemList); //[1,2,4]
    }
}

AttributeSelection.prototype.includeUserSelections = function (userSelection) {
    var checkedAttItemParent = this.parentAtt;

    if (!this.previewDataList[checkedAttItemParent]) {
        this.previewDataList[checkedAttItemParent] = {};
    }

    if (!this.previewDataList[checkedAttItemParent].selectedItems) {
        this.previewDataList[checkedAttItemParent].selectedItems = [];
    }

    if (typeof +userSelection == "number") {
        this.previewDataList[checkedAttItemParent].selectedItems.push(userSelection);
    }
    else {
        alert("Unable to manage selections which are cannot be converted to number");
    }

    /*
    if (!root.previewDataList[checkedAttItemParent]) {
        root.previewDataList[checkedAttItemParent] = {};
        root.previewDataList[checkedAttItemParent].selectedItems = [];
    }*/
}

AttributeSelection.prototype.respondCheckboxChange = function (eventSource) {
    console.log(eventSource);
    var root = this;

    var checkedAttItemParent = root.parentAtt;
    var id = eventSource.target.id.substr("attribute_".length);

    if (eventSource.target.checked) {
        root.includeUserSelections(id);
        root.previewDataList[checkedAttItemParent].isIncluded = $("input[name='groupAttributeShouldBeIncluded']:checked").val() == "include";
    }
    else {
        let array = root.previewDataList[checkedAttItemParent].selectedItems;
        const index = array.indexOf(id);
    
        if (index !== -1) {
            array.splice(index, 1);
            if (array.length == 0) {
                delete root.previewDataList[checkedAttItemParent].selectedItems;
                delete root.previewDataList[checkedAttItemParent].isIncluded;
            }
        }
    }
}

AttributeSelection.prototype.clearSelections = function () {
    var checkedAttItemParent = this.parentAtt;

    // Neither selected nor available in hierarchy
    if (!this.previewDataList[checkedAttItemParent]) {
        return;
    }
    // No selection is made on this attribute yet
    if (!this.previewDataList[checkedAttItemParent].selectedItems) {
        return;
    }

    delete this.previewDataList[checkedAttItemParent].selectedItems;
    delete this.previewDataList[checkedAttItemParent].isIncluded;

    this.filter_items();
}
