/*global AttributeSelection jQuery ColumnWizard document $ Mustache NProgress window Slick alert*/
function CommonMethods() {
}
// Item load via Slick grid
CommonMethods.prototype.loadViaSlickGrid = function (itemTyp, itemObj, parentItem) {
    var grid, options = {}, columns = [], slickdata = [], parent;
    document.getElementById(itemTyp).innerHTML = "";
    parent = parentItem === null ? "" : parentItem;

    for (var i = 0; i < itemObj.length; i++) {
        var obj1 = itemObj[i];
        var itemCode = (itemTyp === "attribute") ? obj1[itemTyp + "Descr"] : obj1[itemTyp + "Code"];
        slickdata[i] = {
            itemPkey: obj1[itemTyp + "Pkey"],
            itemDescr: obj1[itemTyp + "Descr"],
            itemCode: itemCode,
            id: itemTyp,
            parent: parent
        };
    }
    var checkboxSelector = new Slick.CheckboxSelectColumn({});
    columns.push(checkboxSelector.getColumnDefinition());
    columns.push({
        id: "itemDescr",
        name: "",
        field: "itemDescr",
        width: 300
    });
    grid = new Slick.Grid("#" + itemTyp, slickdata, columns, options);
    grid.setSelectionModel(new Slick.RowSelectionModel({ selectActiveRow: false }));
    grid.registerPlugin(checkboxSelector);
    grid.getSelectedRows();
    return grid;
};

// Get checked items [pkey + name]
CommonMethods.prototype.getCheckedItems = function (itemGrid) {
    var resultArray = [];
    for (var i = 0; i < itemGrid.getSelectedRows().length; i++) {
        var obj = {};
        var selectedItemIndex = itemGrid.getSelectedRows()[i];
        obj.rowNumber = selectedItemIndex;  /* Get row number for selected rows -- Oni 21.10.2016*/
        obj.pkey = itemGrid.getDataItem(selectedItemIndex).itemPkey;
        obj.code = itemGrid.getDataItem(selectedItemIndex).itemCode;
        obj.descr = itemGrid.getDataItem(selectedItemIndex).itemDescr;
        resultArray.push(obj);
    }
    /*resultArray = $(".check_" + itemType + ":checkbox:checked").map(function () {
        var obj = {};
        obj.pkey = this.id.slice(this.id.indexOf("_") + 1);
        obj.code = this.value;
        obj.descr = $(this).next("label").text();
        return obj;
    }).get();*/
    return resultArray;
};

// Get checked items pkey [only for building query string to retrieve]
CommonMethods.prototype.getCheckedItemsPkey = function (itemObj) {
    var selectedPkey = [];
    $.each(itemObj, function (index, object) {
        selectedPkey[index] = object.pkey;
    })
    return selectedPkey;
};

// Remove loader after successful ajax response
CommonMethods.prototype.removeLoader = function () {
    $("#loaderPage").css("display", "none");
    $("#homePage").css("visibility", "visible");
};
function WDV() {
    this.attribItemList = new AttributeSelection();
    this.columnWizard = new ColumnWizard();
    this.commonMethod = new CommonMethods();
}
WDV.prototype.PreviousStateInfo = {};
WDV.prototype.querySettings = [];
WDV.prototype.pervSelectedAttTopic = {};
WDV.prototype.queryPkey = null;
WDV.prototype.reportQueryPkey = null;
WDV.prototype.dc_key = null;
WDV.prototype.queryAutoCompleteID = null;


// Load status for Ajax call
WDV.prototype.loadStatus = function () {
    var ajaxStatus = {
        status: [
            { statusName: "Topic List", statusNo: 1 },
            { statusName: "Periods", statusNo: 2 },
            { statusName: "Measure", statusNo: 3 },
            { statusName: "Currency", statusNo: 4 },
            { statusName: "Statistics", statusNo: 5 }
        ]
    },
    templateLoadingStatus = $("#tmplLoadingStatus").html(),
    renderedStatus = Mustache.render(templateLoadingStatus, ajaxStatus);
    $("#loadingStatus").append(renderedStatus);
};
WDV.prototype.fromProfilePage = function () {
    var root = this;
    var id = window.getParameterByName("selectedQueryName");
    this.queryAutoCompleteID = window.getParameterByName("autoCompleteID");
    var dsnName = window.getParameterByName("dsnName");
    if (id !== "undefined") {
        jQuery.ajax({
            type: "POST",
            url: "../api/PADDSLiteReport/readHomeStateConfigById",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ dsnName: dsnName, id: id }),
            dataType: "json",
            success: function (data) {
                //alert(data.d);
                var reportData = data;
                reportData = reportData.split("#");
                root.PreviousStateInfo = JSON.parse(reportData[0]);
                var attrState = JSON.parse(reportData[0]);
                var columnStateObj = JSON.parse(reportData[1]);
                root.attribItemList.previewDataList = attrState;// JSON.parse(attrState);
                root.columnWizard.columnState = columnStateObj;
                root.bodyOnload();

            }
        });
    }
    else {
        this.bodyOnload();
    }

};
// Call function during Body on-load
WDV.prototype.bodyOnload = function () {
    this.loadStatus();
    NProgress.set(0.0);
    var progress = 0;
    var root = this;
    var reducer = function (id) {
        ++progress;
        NProgress.set(progress * 0.2);
        $("#status" + id).prop("checked", true);
        var coverage = progress * 20 + "%";
        $("#bigCounter").html(coverage);

        // todo: count progressCount dynamically
        if (progress === 5) {
            root.commonMethod.removeLoader();
            NProgress.done();
            if (root.attribItemList.previewDataList !== null) {
                root.loadAttributeFromPreviousQuery();
            }
            if (root.columnWizard.columnState.periodType === "custom") {
                document.getElementById("customPeriodTab").checked = true;
            }

            else {
                document.getElementById("generalPeriodTab").checked = true;
            }
            root.columnWizard.previousColumnSelection();
            root.attribItemList.removeCheckBoxFromParentNode();
        }
    };
    this.attribItemList.getAttributeList(reducer);   // Load Attribute
    this.columnWizard.load_columnWizard_structure(); // Load Column Wizard Structure
    this.columnWizard.load_period(reducer);          // Load Period    
    this.columnWizard.load_measure(reducer);		 // Load measure
    this.columnWizard.load_currency(reducer);	     // Load currency
    this.columnWizard.load_stat(reducer);            // Load statistic


};

WDV.prototype.prepareQuerySettings = function () {

    this.attribItemList.reportDataList = JSON.stringify(this.attribItemList.previewDataList);
    var matchedAttributesViaName = function (n) {
        return n.text === attributeName;
    };

    var previewDataSet = jQuery.extend(true, {}, this.attribItemList.previewDataList);;
    for (var attributeName in previewDataSet) {
        for (var j = 0; j < this.attribItemList.attributeTopicList.length; j++) {
            var attributeTopicOject = $.grep(this.attribItemList.attributeTopicList[j].nodes, matchedAttributesViaName);
            if (attributeTopicOject.length > 0) {
                previewDataSet[attributeName].attributePkey = attributeTopicOject[0].attributePkey;
                previewDataSet[attributeName].dbName = attributeTopicOject[0].dbName;
                previewDataSet[attributeName].isGroupedTopic = attributeTopicOject[0].isGroupedTopic;

                if (previewDataSet[attributeName].isGroupedTopic == 1) {
                    previewDataSet[attributeName].groupBaseTopicPkey = attributeTopicOject[0].groupBaseTopicPkey;
                }

                previewDataSet[attributeName].dbSelectColumns = attributeTopicOject[0].dbSelectColumns;
                if (typeof previewDataSet[attributeName].selectedItems !== "undefined") {
                    previewDataSet[attributeName].selectedItems = previewDataSet[attributeName].selectedItems;
                    //this.commonMethod.getCheckedItemsPkey();
                }
                break;
            }
        }
    }

    // Update the sort column
    var checkedItemValue = $("input[name=previewColumnSortSelection_Modal]:checked")[0].value;
    this.columnWizard.columnDataset.columnSettingsForServer[checkedItemValue - 1][5] = true;

    this.querySettings.length = 0;
    this.querySettings.push({
        "selectedColumnItems": this.columnWizard.columnDataset.columnSettingsForServer,
        "selectedAttribItems": previewDataSet
    });

}

WDV.prototype.updateSortSelection = function (userInput) {
    $("input[name=previewColumnSortSelection_Modal][value=" + userInput + "]").prop("checked", "checked");
}

//This method follows chian of responsbility
WDV.prototype.GenerateTable = function () {
    this.attribItemList.selectedAttributeItems();
    this.attribItemList.getUpdatedDataforTreeView();

    var columnhasError = this.columnWizard.validateColumnSelection();// CheckForColumnSelectionAlert

    var attributeHasError = this.attribItemList.validateAttributeSelection();//checkFofEmptySelection

    if (!columnhasError && !attributeHasError) {
        this.attribItemList.previewSelectedattribute();// remove data preparing dependency from view part of this method
        if (document.getElementById("table_toggle").checked) {

        }
        else {
            var selectionStatus = this.columnWizard.previewSelectedcolumns();// remove data preparing dependency from view part of this method
            if (selectionStatus == false) {
                return;
            }
        }
        this.prepareQuerySettings();


        this.getBaseTable();
        $('#myModal').modal();
    }


};
// getBaseTableName
WDV.prototype.baseTableName = null;

WDV.prototype.getBaseTable = function () {
    var root = this;
    var topicAttributePkeyList = [];
    var dsnName = window.getParameterByName("dsnName");

    for (var attributeName in this.querySettings[0].selectedAttribItems) {
        /*if (this.querySettings[0].selectedAttribItems[attributeName].isGroupedTopic == 1) {
            topicAttributePkeyList.push(this.querySettings[0].selectedAttribItems[attributeName].groupBaseTopicPkey);
        }
        else {
            topicAttributePkeyList.push(this.querySettings[0].selectedAttribItems[attributeName].attributePkey);
        }
        */
        topicAttributePkeyList.push(this.querySettings[0].selectedAttribItems[attributeName].attributePkey);

    }
    var measureList = this.commonMethod.getCheckedItems(this.columnWizard.measureGrid);
    var measureCodeList = [];
    for (var i = 0; i < measureList.length; i++) {
        var s = measureList[i];
        measureCodeList.push(s.code);
    }

    // Disable retrieve until base table is not resolved
    $("#retrieveInit").attr("disabled", "disabled");
    jQuery.ajax({
        type: "POST",
        url: "../api/PADDSLiteReport/ResolveFactTableWithDCKey",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            dsnName: dsnName,
            attributeTopicPkey: topicAttributePkeyList.join(";"),
            measureCode: measureCodeList.join(";"),
        }),

        // find out baseTableaName and measureCodes from response data 
        success: function (data) {
            var successResult = data;
            var results = successResult.Key.split(" ");
            // root.saveQueryAndRedirect(results[0]);
            root.baseTableName = results[0];
            root.dc_key = successResult.Value;

            $("#retrieveInit").removeAttr("disabled");
        }

    });
};

// User wants to save/update the query.
WDV.prototype.storeCurrentQuery = function () {

    // Situations to tackle:
    // name already exists?
    // new query save, won't retrieve yet
    // existing query, retrieving with changes
    // existing query, retrieve without changes
    this.GenerateTable();
    var queryID = this.queryAutoCompleteID;

    // new query, need to wait till previewing. Need saved report id to store name.
    if (queryID == null || isNaN(queryID) || queryID == -1) {
        queryID = -1;
        this.saveQueryForQueryID(1);
    }

        // Otherwise, already we know the queryID, just update its name
    else {
        this.saveQueryForQueryID(2);
    }
}

// Save the query and re-direct to report page
WDV.prototype.saveQueryWithPlan = function (queryID, rowCount) {
    if (rowCount > 100) {
        if (confirm("Your query involves approximately: " + rowCount + " rows. Do you want to continue?")) {
            window.location.href = '../View/ReportPage_5' + '?dsnName=' + window.getParameterByName("dsnName") + "&qid=" + queryID + "&selectedQuery=" + this.queryPkey + "&autoCompleteID=" + this.queryAutoCompleteID;
            //window.location.href = "report2.aspx?dsnName=" + window.getParameterByName("dsnName") + "&qid=" + queryID + "&selectedQuery="+this.queryPkey;
            //location.href = '@Url.Action("HomePage", "View")?dsnName=' + dsnName + '&selectedQueryName=' + selectedQueryName;

        }
    }
    else
        window.location.href = '../View/ReportPage_5' + '?dsnName=' + window.getParameterByName("dsnName") + "&qid=" + queryID + "&selectedQuery=" + this.queryPkey + "&autoCompleteID=" + this.queryAutoCompleteID;
}

// private function to store query details in a page and return inserted/updated queryID
WDV.prototype.saveQueryForQueryID = function (nextOperation) {
    if (this.baseTableName === null)
        alert("Cache exhausted. Please wait some time and try again.");
    var root = this;
    var dsnName = window.getParameterByName("dsnName");
    var columnSelection = this.columnWizard.GetSelectedColumn();
    var queryID = 0;

    jQuery.ajax({

        type: "POST",
        url: "../api/PADDSLiteReport/QueryPlanForRetrieve",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            dsnName: dsnName,
            queryStatement: JSON.stringify(this.querySettings[0]),

            userChoiceAttribute: JSON.stringify(this.querySettings[0].selectedAttribItems),
            userChoiceColumns: columnSelection,

            baseTableName: root.baseTableName,
            dcKey: root.dc_key,
        }),
        success: function (data) {
            var queryInfo = JSON.parse(data);
            root.reportQueryPkey = queryInfo.queryPkey;
            //root.saveQueryOnreportGeneration(queryInfo.rowCountEstimated);
            //root.saveQueryWithPlan(root.reportQueryPkey, queryInfo.rowCountEstimated);
            qname = document.getElementById("fileName").value;
            if (nextOperation === 1) {
                root.queryAutoCompleteID = queryInfo.queryPkey;
                jQuery.ajax({
                    type: "POST",
                    url: "../api/PADDSLiteReport/storeQuery",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        queryName: qname,
                        newQueryID: queryInfo.queryPkey,
                        existingQueryID: -1,
                        dsnName: dsnName
                    }),
                    success: function (data) {
                        alert("Query Saved..!!");
                        $('#myModal').modal();
                    }
                });
            }
            else if (nextOperation === 2) {
                qname = document.getElementById("fileName").value;
                jQuery.ajax({
                    type: "POST",
                    url: "../api/PADDSLiteReport/storeQuery",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        queryName: qname,
                        newQueryID: queryInfo.queryPkey,
                        existingQueryID: root.queryAutoCompleteID,
                        dsnName: dsnName
                    }),
                    success: function (data) {
                        alert(data);
                    }
                });
            }
            else {
                root.saveQueryWithPlan(queryInfo.queryPkey, queryInfo.rowCountEstimated);
            }
        },
        error: function (textStatus, errorThrown) {
            $.LoadingOverlay("hide");
            Materialize.toast(textStatus.responseJSON.ExceptionMessage, 2000, "rounded");
        }
    });
};

WDV.prototype.saveQueryAndRedirect = function () {


    if (this.baseTableName === null)
        alert("Cache exhausted. Please close this modal and try to preview again");
    else {
        var root = this;
        var dsnName = window.getParameterByName("dsnName");
        var columnSelection = this.columnWizard.GetSelectedColumn();
        var queryID = 0;
        $.LoadingOverlay("show");
        jQuery.ajax({

            type: "POST",
            url: "../api/PADDSLiteReport/QueryPlanForRetrieve",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                dsnName: dsnName,
                queryStatement: JSON.stringify(this.querySettings[0]),

                userChoiceAttribute: JSON.stringify(this.querySettings[0].selectedAttribItems),
                userChoiceColumns: columnSelection,

                baseTableName: root.baseTableName,
                dcKey: root.dc_key,
            }),
            success: function (data) {
                var queryInfo = JSON.parse(data);
                root.reportQueryPkey = queryInfo.queryPkey;
                //root.saveQueryOnreportGeneration(queryInfo.rowCountEstimated);
                $.LoadingOverlay("hide");
                root.saveQueryWithPlan(root.reportQueryPkey, queryInfo.rowCountEstimated);
            },
            error: function (textStatus, errorThrown) {
                $.LoadingOverlay("hide");
                Materialize.toast(textStatus.responseJSON.ExceptionMessage, 2000, "rounded");
            }
        });

    }


};


//Toggling column table overview 
WDV.prototype.toggleColumnPreviewTable = function () {
    if (document.getElementById("table_toggle").checked) {
        if (document.getElementById("column_table") === null) {
            //this.GenerateTable();
            this.columnWizard.previewSelectedcolumns();
        }
        document.getElementById("columnWizardTable").style.display = "block";
        document.getElementById("mybtn").disabled = false;
        //document.getElementById("report").style.visibility = "visible";
        //document.getElementById("nav_report").style.visibility = "visible";
    }
    else
        document.getElementById("columnWizardTable").style.display = "none";
    document.getElementById("columnStatus").style.display = "none";
};

WDV.prototype.getAttributeNodeIdFromUI = function (nodeName) {

    var listOfAttribute = $(document.getElementById("treeview-checkable").getElementsByTagName("li"));
    for (var i = 0; i < listOfAttribute.length; i++) {
        if ($(listOfAttribute[i]).text().trim().includes(nodeName)) {
            return $(listOfAttribute[i]).data('nodeid');

        }
    }
};
WDV.prototype.loadAttributeFromPreviousQuery = function () {
    var attInhierarcy = [];
    $(".statusCheckbox").prop("checked", false);
    for (property in this.attribItemList.previewDataList) {
        if (this.attribItemList.previewDataList[property].isInHierarchy === true) {
            var position = this.getAttributeNodeIdFromUI(property);
            $("#treeview-checkable").treeview('checkNode', [position, { silent: true }]);
            attInhierarcy[this.attribItemList.previewDataList[property].positionInQuery] = property;

        }
    }

    for (var k = 0; k < attInhierarcy.length; k++) {
        if (!this.attribItemList.attributeTopicIsInHirarchy(attInhierarcy[k]))
            this.attribItemList.moveAttributeTxtToHierarchy(attInhierarcy[k]);
    }
};

WDV.prototype.generateQueryStateData = function () {
    var fileName = document.getElementById("fileName").value;

    this.attribItemList.reportDataList = JSON.stringify(this.attribItemList.previewDataList);
    var matchedAttributesViaName = function (n) {
        return n.text === attributeName;
    };
    var previewDataSet = jQuery.extend(true, {}, this.attribItemList.previewDataList);;
    for (var attributeName in previewDataSet) {
        for (var j = 0; j < this.attribItemList.attributeTopicList.length; j++) {
            var attributeTopicOject = $.grep(this.attribItemList.attributeTopicList[j].nodes, matchedAttributesViaName);
            if (attributeTopicOject.length > 0) {
                previewDataSet[attributeName].attributePkey = attributeTopicOject[0].attributePkey;
                previewDataSet[attributeName].dbName = attributeTopicOject[0].dbName;
                previewDataSet[attributeName].isGroupedTopic = attributeTopicOject[0].isGroupedTopic;
                if (previewDataSet[attributeName].isGroupedTopic == 1) {
                    previewDataSet[attributeName].groupBaseTopicPkey = attributeTopicOject[0].groupBaseTopicPkey;
                }
                previewDataSet[attributeName].dbSelectColumns = attributeTopicOject[0].dbSelectColumns;
                if (typeof previewDataSet[attributeName].selectedItems !== "undefined") {
                    previewDataSet[attributeName].selectedItems =
                    this.commonMethod.getCheckedItemsPkey(previewDataSet[attributeName].selectedItems);
                }
                break;
            }
        }
    }

    this.querySettings.length = 0;
    this.querySettings.push({
        "selectedColumnItems": this.columnWizard.columnDataset.columnSettingsForServer,
        "selectedAttribItems": previewDataSet
    });

    var root = this;
    var dsnName = window.getParameterByName("dsnName");

}

// Obsolete?
WDV.prototype.saveQueryOnreportGeneration = function (rowCountFromServer) {
    var root = this;
    var fetchRowCount = function () {
        var min = 10, max = 2000;
        var rnd = Math.floor(Math.random() * (max - min + 1)) + min;
        root.saveQueryWithPlan(root.reportQueryPkey, rowCountFromServer);
        /*
        jQuery.ajax({

            type: "POST",
            url: "../../WebService1.asmx/getPlan",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                dsnName: dsnName,
                queryID: queryID,
            }),
            success: function (data) {  
                root.saveQueryWithPlan(queryID, data.d);

            }
        });*/
    }
    this.generateQueryStateData();
    var columnSelection = this.columnWizard.GetSelectedColumn();
    var dsnName = window.getParameterByName("dsnName");
    jQuery.ajax({

        type: "POST",
        url: "../api/PADDSLiteReport/saveQuery",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            dsnName: dsnName,
            homeSateSelection: JSON.stringify(this.attribItemList.reportDataList),
            columnStateConfig: columnSelection,
            fileName: "N/A"
        }),
        success: function (data) {
            root.queryPkey = data.d;
            fetchRowCount();
        }
    });
    return root.queryPkey;
}
WDV.prototype.saveQuery = function () {
    //var date = document.getElementById("dateTime").value;
    //var comment = document.getElementById("comment").value;
    var dsnName = window.getParameterByName("dsnName");
    var fileName = document.getElementById("fileName").value;
    this.generateQueryStateData();
    var columnSelection = this.columnWizard.GetSelectedColumn();
    this.generateQueryStateData();
    var root = this;
    if (this.queryPkey === null) { // save report state
        jQuery.ajax({

            type: "POST",
            url: "../api/PADDSLiteReport/saveQuery",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                dsnName: dsnName,
                homeSateSelection: JSON.stringify(this.attribItemList.reportDataList),
                columnStateConfig: columnSelection,
                fileName: fileName
            }),
            success: function (data) {
                root.queryPkey = data.d;
                alert("Query Saved..!!");
                $('#myModal').modal();

            }
        });
    }
    else {
        jQuery.ajax({

            type: "POST",
            url: "../api/PADDSLiteReport/updateQuery",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                dsnName: dsnName,
                homeSateSelection: JSON.stringify(this.attribItemList.reportDataList),
                columnStateConfig: columnSelection,
                fileName: fileName,
                queryPkey: root.queryPkey
            }),
            success: function (data) {
                root.queryPkey = data.d;
                alert("Query Updated..!!");
                $('#myModal').modal();

            }
        });

    }

};
// isi not functional
WDV.prototype.saveLink = function () {
    if (this.baseTableName === null) alert("Base table not found. Please contact support");
    var root = this;
    var dsnName = window.getParameterByName("dsnName");
    var columnSelection = this.columnWizard.GetSelectedColumn();
    jQuery.ajax({

        type: "POST",
        url: "../../WebService1.asmx/saveQueryStatement",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            dsnName: dsnName,
            queryStatement: JSON.stringify(this.querySettings[0]),
            baseTableName: root.baseTableName,
            dcKey: root.dc_key,
        }),
        success: function (data) {
            var insertedID = data.d;
            alert("Inserted at row number " + insertedID);

        }
    });
};

