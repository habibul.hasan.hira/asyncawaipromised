/*global jQuery $ Mustache CommonMethods document window*/


function ColumnWizard() {
    //periodGrid
    this.commonMethod = new CommonMethods();
}

// Variable List
ColumnWizard.prototype.customPeriodLoadViaSlick = false;
ColumnWizard.prototype.periodObject = {};
ColumnWizard.prototype.statObject = {};
ColumnWizard.prototype.measureObject = {};
ColumnWizard.prototype.currencyObject = {};
ColumnWizard.prototype.columnDataset = {};
ColumnWizard.prototype.columnState = {};
ColumnWizard.prototype.checkedPeriod = [];
ColumnWizard.prototype.periodGrid=null;
ColumnWizard.prototype.measureGrid=null;
ColumnWizard.prototype.currencyGrid=null;
ColumnWizard.prototype.statisticGrid=null;

// Load HTML structure of column wizards
ColumnWizard.prototype.load_columnWizard_structure = function () {
    var columnCategory = {
        columns: [
            { name: "Measure", id: "measure", icon: "equalizer" },
            { name: "Currency", id: "currency", icon: "attach_money" },
            { name: "Statistic", id: "percent", icon: "timeline" }
        ]
    },
    templateColumnStructure = $("#tmplColumnStructure").html(),
    renderedColumnStructure = Mustache.render(templateColumnStructure, columnCategory);

    $("#columnStructure").append(renderedColumnStructure);
};

// Load Periods object
ColumnWizard.prototype.load_period = function (end) {
    if (typeof this.periodObject.length === "undefined") {
        var root = this;
        jQuery.ajax({
            type: "GET",
            url: "../api/PADDSLiteReport/getPeriods?dsnName="+ window.getParameterByName("dsnName"),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                root.periodObject = data;
                if (root.columnState.periodType === "custom")
                    root.customPeridoLoad();
               // else
                    root.generalPeriodTypeLoad();
                    //Load Period type in General Tab once
                end(2);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $(".error2").css("visibility", "visible");
                $(".error2").text(errorThrown);
            }
        });
    }
};

// Load Period type in "General" Tab
ColumnWizard.prototype.generalPeriodTypeLoad = function () {
    var standardType = [], tempArr = [], relativeType = [], tempObj = {}, final = {};

    var PeriodList = this.periodObject.map(function (obj) {
        var PeriodObj = {};

        PeriodObj.periodTyp = obj.distinctPeriodTypes;
        PeriodObj.isRel2ld = obj.isRel2ld;
        return PeriodObj;
    });

    var distinctPeriodTypes = PeriodList.reduce(function (field, e1) {
        var matches = field.filter(function (e2) { return e1.periodTyp === e2.periodTyp; });
        if (matches.length === 0) {
            field.push(e1);
        } return field;
    }, []);

    for (var i = 0; i < distinctPeriodTypes.length; i++) {
        if (distinctPeriodTypes[i].isRel2ld === 0) {
            standardType.push(distinctPeriodTypes[i]);
        }
        else {
            relativeType.push(distinctPeriodTypes[i]);
        }
    }
    tempObj.periodList = standardType;
    tempObj.isRelative = false;
    tempObj.type = "standard";
    tempArr.push(tempObj);
    tempObj = {};
    tempObj.periodList = relativeType;
    tempObj.isRelative = true;
    tempObj.type = "relative";
    tempArr.push(tempObj);

    final.periodType = tempArr;

    var templateGeneralPeriod = $("#tmplGeneralPeriodList").html(),
        renderedGeneralPeriod = Mustache.render(templateGeneralPeriod, final);

    $("#generalPeriodList").append(renderedGeneralPeriod);
    $("ul.tabs").tabs();
};

// Choose period selection option
ColumnWizard.prototype.period_selection = function () {
    if (document.getElementById("generalPeriodTab").checked) {
        document.getElementById("customPeriodBlock").style.display = "none";
        document.getElementById("generalPeriodBlock").style.display = "block";
       
        //this.statisticGrid = this.commonMethod.loadViaSlickGrid("percent", this.statObject);
    }
    else {
        document.getElementById("generalPeriodBlock").style.display = "none";
        document.getElementById("customPeriodBlock").style.display = "block";
        document.getElementById("cus_std").checked = true;
        if (!this.customPeriodLoadViaSlick) {
            this.periodGrid = this.commonMethod.loadViaSlickGrid("period", this.periodObject, null);
            this.customPeriodLoadViaSlick = true;
            if (!!this.columnState !== false) {
                this.previewSelectedcolumns();
            }
        }
        //this.filter_periods_and_stat();
        //this.statisticGrid = this.commonMethod.loadViaSlickGrid("percent", this.statObject);
    }

};

ColumnWizard.prototype.isFunctional = function (stat) {

    var openOption = ["","MS", "Growth"]; // if functional add stat percentCode to this list
    
    return openOption.indexOf(stat.percentCode) !== -1;

    /*if ((openOption.filter(function (statItem) { return (statItem === stat.percentCode); }).length>0)) 
        return true;
    else 
        return false;*/
    

}

// Load Statistic
ColumnWizard.prototype.load_stat = function (end) {
    if (typeof this.statObject.length === "undefined") {
        var root = this;
        jQuery.ajax({
            type: "GET",
            url: "../api/PADDSLiteReport/getPercent?dsnName="+ window.getParameterByName("dsnName"),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
               
                root.statObject = data.filter(function (statItem) { return root.isFunctional(statItem) });
                root.statisticGrid = root.commonMethod.loadViaSlickGrid("percent", root.statObject, null);
                
               
               
                end(5);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $(".error5").css("visibility", "visible");
                $(".error5").text(errorThrown);
            }
        });
    }
};

// Load Measure
ColumnWizard.prototype.load_measure = function (end) {
    if (typeof this.measureObject.length === "undefined") {
        var root = this;
        jQuery.ajax({
            type: "GET",
            url: "../api/PADDSLiteReport/getValue?dsnName="+ window.getParameterByName("dsnName"),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                root.measureObject = data.filter(function (measureItem) { return (!measureItem.measureDescr.startsWith("Latest")) });
                root.measureGrid = root.commonMethod.loadViaSlickGrid("measure", root.measureObject, null);
                end(3);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $(".error3").css("visibility", "visible");
                $(".error3").text(errorThrown);
            }
        });
    }
};

// Load Currency
ColumnWizard.prototype.load_currency = function (end) {
    if (typeof this.currencyObject.length === "undefined") {
        var root = this;
        jQuery.ajax({
            type: "GET",
            url: "../api/PADDSLiteReport/getCurrency?dsnName="+ window.getParameterByName("dsnName"),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                root.currencyObject = data;
                root.currencyGrid = root.commonMethod.loadViaSlickGrid("currency", root.currencyObject, null);
                end(4);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $(".error4").css("visibility", "visible");
                $(".error4").text(errorThrown);
            }
        });
    }
};
ColumnWizard.prototype.getMeasureWithMoney = function (selectedMeasure) {

    var measureWithMoneyList=[];
    for(var i=0;i<selectedMeasure.length;i++)
    {
        measureWithMoneyList.push(this.measureObject.find((item)=>{return item.measureCode===selectedMeasure[i].code}))

    }
    return measureWithMoneyList.length;
}

ColumnWizard.prototype.validateColumnSelection = function () {
    var hasError = false;
    this.selectedColumnList();
    if (document.getElementById("generalPeriodTab").checked) {
        var periodType = $("input:radio[name='generalPeriodType']:checked").val();
        var noOfLastPeriod = $("#noOfLastPeriod").val();
        if (periodType === undefined || noOfLastPeriod === "")
        {
            Materialize.toast("Select General Period Type and No of Last Preiod", 2000, "rounded");
            hasError = true;
            return hasError;
        }
    }
    if ($('#customPeriodTab')[0].checked === true && this.checkedPeriod.length === 0)
    {
        Materialize.toast("Please select at least one  Preiod", 2000, "rounded");
        hasError = true;
        return hasError;
    }

    
    var selectedMeasure = this.commonMethod.getCheckedItems(this.measureGrid);
    if(selectedMeasure.length===0)
    {
        Materialize.toast("Please select at least one measure", 1000, "rounded");
        hasError = true;
        return hasError;
    }

    var checkedCurr = this.commonMethod.getCheckedItems(this.currencyGrid);
    for (var i = 0; i < selectedMeasure.length; i++) {
        var isMoneyDependant =
            wdv.columnWizard.measureObject.filter(x=>x.measureCode == selectedMeasure[i].code)[0].isMoney;

        if(isMoneyDependant && checkedCurr.length == 0){
            Materialize.toast("Please select at least one currency for " + selectedMeasure[i].descr, 1000, "rounded");
            hasError = true;
            return hasError;
        }
    }
    
    var checkedStat = this.commonMethod.getCheckedItems(this.statisticGrid);
    if (checkedStat.length === 0)
    {
        Materialize.toast("Please select at least one statistics", 1000, "rounded");
        hasError = true;
    }
    
    return hasError;
}



// Filter periods in Custom Tab and corresponding statistic on click event of radio button
ColumnWizard.prototype.filter_periods_and_stat = function () {
    var flag,
    searchString = $("#search_per").val(),
    PeriodListBySearch = this.periodObject.filter(function (value) {
        var filter = new RegExp(searchString, "gi");
        return value.periodDescr.match(filter);
    });

    if (document.getElementById("cus_std").checked) flag = 0;
    else flag = 1;

    var filteredPeriodList = PeriodListBySearch.filter(function (value) {
        return value.isRel2ld == 0; // Consider only STD periods now (value.isRel2ld === flag);
    });

    var filteredStatList = this.statObject.filter(function (value) {
        return true;// (value.prlSupported === flag);
    });

    this.periodGrid = this.commonMethod.loadViaSlickGrid("period", filteredPeriodList);
    //this.statisticGrid = this.commonMethod.loadViaSlickGrid("percent", filteredStatList);
};

// Generate List for selected items from Column Wizard
ColumnWizard.prototype.selectedColumnList = function () {
    var columnSettingsForDisplay = [], columnSettingsForServer = [];
    var filteredPeriod = null;
    this.checkedPeriod = [];

    // Compute timeperiods. 
    // Todo: apply better algorithm to find filteredPeriod
    if (document.getElementById("generalPeriodTab").checked) {
        var periodType = $("input:radio[name='generalPeriodType']:checked").val();
        var noOfLastPeriod = $("#noOfLastPeriod").val();

        filteredPeriod = this.periodObject.filter(function (el) {
            return (el.distinctPeriodTypes === periodType);
        });

        for (var i = 0; i < noOfLastPeriod; i++) {
            var obj = {};
            obj.pkey = filteredPeriod[i].periodPkey;
            obj.code = filteredPeriod[i].periodCode;
            obj.descr = filteredPeriod[i].periodDescr;
            obj.yearBeforeRef = filteredPeriod[i].yearBeforeRef;
            this.checkedPeriod.push(obj);
        }
        var sortOrder = $("input[type='radio'][name='sortOrder']:checked").val();

        if (sortOrder === "asc") {
            this.checkedPeriod.sort(function (x, y) { return x.pkey < y.pkey; });
        }
    }
    else {
        this.checkedPeriod = this.commonMethod.getCheckedItems(this.periodGrid);
        filteredPeriod = this.periodObject;
        for (var k = 0; k < this.periodObject.length; k++) {
            for (var j = 0; j < this.checkedPeriod.length; j++) {
                if (this.periodObject[k].periodPkey === this.checkedPeriod[j].pkey) {
                    this.checkedPeriod[j].yearBeforeRef = this.periodObject[k].yearBeforeRef;
                }
            }
        }
    }

    // Compute statistics
    var checkedStat = this.commonMethod.getCheckedItems(this.statisticGrid);
    var checkedVal = this.commonMethod.getCheckedItems(this.measureGrid);
    var checkedCurr = this.commonMethod.getCheckedItems(this.currencyGrid);
    //||(checkedCurr.length === 0)s
    if ((this.checkedPeriod.length === 0) ||
        (checkedStat.length === 0) ||
        (checkedVal.length === 0) 
        ) {
        $("#popup-content").html("Columns are not designed yet");
        document.getElementById("columnStatus").style.display = "block";
        return;
    }

    // Prepare column display rows
    columnSettingsForDisplay.push(["Measures", "Periods", "Currency", "Statistic", "YearBeforeRef", "isSortColumn"]);

    var loopCurrency = function (mCode, mDesc, pCode, pDesc, s, yearBeforeRef) {
        for (var i = 0; i < checkedCurr.length; i++) {
            columnSettingsForServer.push([mCode, pCode, checkedCurr[i].code, s, yearBeforeRef, false]);
            columnSettingsForDisplay.push([mDesc, pDesc, checkedCurr[i].descr, s, yearBeforeRef, false]);
        }
    };

    var checkedValueDetection = function (e2) {
        return e2.measureCode === checkedVal[m].code;
    };

    for (var m = 0; m < checkedVal.length; m++) {
        var measureObject = this.measureObject.filter(checkedValueDetection);
        var measureDescr = checkedVal[m].descr;
        var measureCode = checkedVal[m].code;
        // latest-price
        if (measureObject[0].isTimeDependent === 0) {
            //todo: consider currency
            loopCurrency(measureCode, measureDescr, null, null, null, null);
            continue;
        }
        // time dependent specific measures
        for (var p = 0; p < this.checkedPeriod.length; p++) {
            var periodDescr = this.checkedPeriod[p].descr;
            var periodCode = this.checkedPeriod[p].code;
            var yearBeforeRef = this.checkedPeriod[p].yearBeforeRef;
            var yearBeforeRefCode = null;

            var filterForYearBeforeRef = filteredPeriod.filter(function (e) {
                if (e.periodPkey === yearBeforeRef)
                    return e;
            });
            if (filterForYearBeforeRef.length > 0) {
                yearBeforeRefCode = filterForYearBeforeRef[0].periodCode;
            }

            // Loop over stats since atleast 1 item is selected into Stat
            for (var statCount = 0; statCount < checkedStat.length; statCount++) {
                var statDescr = checkedStat[statCount].descr;

                // currency measures (e.g. usd-mnf)
                if (measureObject[0].isMoney === 1) {
                    loopCurrency(measureCode, measureDescr, periodCode, periodDescr, statDescr, yearBeforeRefCode);
                }
                    // currency indepedent measures (e.g. units)
                else {
                    columnSettingsForDisplay.push([measureDescr, periodDescr, null, statDescr, yearBeforeRefCode, false]);
                    columnSettingsForServer.push([measureCode, periodCode, null, statDescr, yearBeforeRefCode, false]);
                }
            }

        }
    }
    this.columnDataset.columnSettingsForServer = columnSettingsForServer;
    return columnSettingsForDisplay;
};

// Create table for Column Selection preview
ColumnWizard.prototype.previewSelectedcolumns = function () {
    if (document.getElementById("table_toggle").checked == false) {
        Materialize.toast("Please click on Show table to select sorting column", 500);
        return false;
    }
    var settingsForDisplay = [];
    settingsForDisplay = this.selectedColumnList();   // Generate selected Column List

    var table = document.createElement("TABLE");
    table.border = "1px solid black";
    table.width = "100%";
    table.style.border = "1px solid #3c8dbc";
    table.setAttribute("class", "centered striped responsive-table");
    table.setAttribute("id", "column_table");

    //Get the count of columns.
    if (settingsForDisplay !== undefined)
    {
    }
    var columnCount = settingsForDisplay[0].length;

    //Add the header row.
    var row = table.insertRow(-1);
    for (var i = 0; i < columnCount; i++) {
        var headerCell = document.createElement("TH");
        headerCell.style.color = "white";
        headerCell.style.backgroundColor = "#00A1E2";
        headerCell.style.textAlign = "center";
        headerCell.innerHTML = settingsForDisplay[0][i];
        row.appendChild(headerCell);
    }

    //Add the data rows for preview area and modal. Starting with 1 to avoid TH rows on top
    for (i = 1; i < settingsForDisplay.length; i++) {
        row = table.insertRow(-1);
        var cell = null;
        for (var j = 0; j < columnCount-1; j++) {
            cell = row.insertCell(-1);
            cell.innerHTML = settingsForDisplay[i][j];
        }
        cell = row.insertCell(-1);
        cell.innerHTML = '<p><input id="previewColumnSortSelection_row' + i + '" type="radio" onchange="wdv.updateSortSelection(this.value)" name="previewColumnSortSelection" value="' + i
            + '"><label for="previewColumnSortSelection_row' + i + '"></label></p>';
    }

    var createTable = document.getElementById("createTable");
    createTable.innerHTML = "";
    createTable.appendChild(table);


    // Rebuild preview Modal's table. If "Show Table" switch is on, then the user can select sort column. So, we need to respect that possibility while re-genrating the table rows in the Modal.
    var previewTable = $(table).clone()[0];
    var sortColumnIndex = 5; // last column's index
    var checkedValue = $("input[name=previewColumnSortSelection]:checked").val();
    checkedValue = !checkedValue == true ? 1 : checkedValue;

    for (var i = 1; i < previewTable.rows.length; i++) {
        var cell = previewTable.rows[i].cells[sortColumnIndex];
        //var checkedValue = checkedValue == i;
        var checkedStatus = checkedValue == i ? "checked" : "";
        cell.innerHTML = '<p><input disabled="disabled" id="previewColumnSortSelection_row' + i + '_Modal" type="radio" ' + checkedStatus
            + ' name="previewColumnSortSelection_Modal" value="' + i + '"><label for="previewColumnSortSelection_row' + i + '_Modal"></label></p>';
    }
    $("#popup-content").html(previewTable);
};

ColumnWizard.prototype.GetSelectedColumn = function () {
    this.selectedColumnList();
    var columnList = {};
    var statList = this.commonMethod.getCheckedItems(this.statisticGrid);
    var measureLsit = this.commonMethod.getCheckedItems(this.measureGrid);
    var currList = this.commonMethod.getCheckedItems(this.currencyGrid);
    columnList.stat= statList;
    columnList.measure = measureLsit;
    columnList.currList = currList;
    if (document.getElementById("generalPeriodTab").checked) {
        columnList.periodType = "general";
        columnList.generalPeriod = this.getGeneralPeriodsettings();
        columnList.noOfLastPeriod = document.getElementById("noOfLastPeriod").value;
    }
    else {
        columnList.periodList = this.checkedPeriod;
        columnList.periodType = "custom";
    }
    var str = JSON.stringify(columnList);
    //return str.replace(/[]/g, '');
    return str;
};

ColumnWizard.prototype.previousColumnSelection = function () {
    var stat = [0];
    if (this.columnState !== null)
    {
        
        var measure = [];
        var curr = [];
        var periodList = [];
        
        for (columnItem in this.columnState)
        {
            for (var i = 0; i < this.columnState[columnItem].length; i++)
            {
                if (columnItem === "stat") stat.push(this.columnState[columnItem][i].rowNumber);
                if (columnItem === "measure") measure.push(this.columnState[columnItem][i].rowNumber);
                if (columnItem === "currList") curr.push(this.columnState[columnItem][i].rowNumber);
                if (this.columnState["periodType"] !== "general") {
                    if (columnItem === "periodList") periodList.push(this.columnState[columnItem][i].rowNumber);
                }
               
            }
        }
        
        this.measureGrid.setSelectedRows(measure);
        this.currencyGrid.setSelectedRows(curr);
        var type = null;
        if (this.columnState["periodType"] == "general") {
            type = this.columnState["generalPeriod"];
            var generalPeriodType = document.getElementsByName("generalPeriodType");
            for (var i = 0; i < generalPeriodType.length; i++) {     
                if (generalPeriodType[i].value === type) {
                    generalPeriodType[i].checked = true;
                }
            }
            document.getElementById("noOfLastPeriod").value = this.columnState["noOfLastPeriod"];
        }
        else if (this.columnState["periodType"] == "custom")
            this.periodGrid.setSelectedRows(periodList);
        
        //custom pannel seelction 
       
    }
    this.statisticGrid.setSelectedRows(stat);


};

ColumnWizard.prototype.customPeridoLoad = function () {

    document.getElementById("generalPeriodBlock").style.display = "none";
    document.getElementById("customPeriodBlock").style.display = "block";
    document.getElementById("cus_std").checked = true;
    if (!this.customPeriodLoadViaSlick) {
        this.periodGrid = this.commonMethod.loadViaSlickGrid("period", this.periodObject, null);
        this.customPeriodLoadViaSlick = true;
        
    }
};

ColumnWizard.prototype.getGeneralPeriodsettings = function ()

{
    var generalPeriodType = document.getElementsByName("generalPeriodType");
    var selectedPeriod = null;
    for (var i = 0; i < generalPeriodType.length; i++) {
        if (generalPeriodType[i].checked == true) {
            selectedPeriod = generalPeriodType[i].value;
        }
    }
    return selectedPeriod;
}