function f (x, y, ...a) {
    return (x + y) * a[a.length-1]
}
console.log(f(1, 2, "hello", true, 3) === 9);
